import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

class Cezar {
    
    /*
    function for input data for scripting
    @param 
    @return string
    */
    public String getString(){
	Scanner in = new Scanner(System.in);
	//InputStreamReader streamReader = new InputStreamReader(System.in);
	//BufferedReader in = new BufferedReader(streamReader);
	return in.nextLine();
    }
	
    /*
    function for check element as copusition of big letters from alphabet
    @param element
    @return true/false
    */
    public boolean isAlphaBig(char el){
	if(el > 64 && el < 91){
	    return true;
	}
	return false;
    }

    /*
    function for check element as copusition of small letters from alphabet
    @param element
    @return true/false
    */
    public boolean isAlphaSmall(char el){
	if(el > 96 && el < 123){
	    return true;
	}
	return false;
    }

   /*
    function forbalance index element with respect to alphabet
    @param index of element
    @return true index element
    */
    public int balanceIndex(int indexLetters){
	while(indexLetters > 25){
	    indexLetters -= 25;
	}
	return indexLetters;
    }

   /*
    function for scripting element for big element
    @param element and key
    @return new element
    */
    public char scriptingBigletter(char letter, int key){
	int indexLetter = (int)letter - 64 + key;
	indexLetter = balanceIndex(indexLetter);
	//System.out.println(indexLetter);
	return (char)(indexLetter + 64);
    }

   /*
    function for scripting element for small element
    @param element and key
    @return new element
    */
    public char scriptingSmallletter(char letter, int key){
	//System.out.println(letter);
	int indexLetter = (int)letter - 96 + key;
	//System.out.println(letter);
	indexLetter = balanceIndex(indexLetter);
	return (char)(indexLetter + 96);
    } 
   
    /*
    function for scripting text
    @param text and key
    @return script text by Ceasar
    */

    public String scripting(String text, int key){
	String script = "";
	for(int i = 0; i < text.length(); i++){
	    if(isAlphaBig(text.charAt(i))){
		//System.out.println(text.charAt(i));
		script += scriptingBigletter(text.charAt(i), key);
		//System.out.println(scriptingBigletter(text.charAt(i), key));
	    } else if(isAlphaSmall(text.charAt(i))){
		script += scriptingSmallletter(text.charAt(i), key);
	    } else {
		script += text.charAt(i);
	    }
	}
	return script;
    }
}
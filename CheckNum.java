
class CheckNum{
	
	/*
    function for check (string == int)
    @param string
    @return true/false
    */
    public boolean isInt(String str){
	int num;
	try {
	    num = Integer.parseInt(str);
	} catch(NumberFormatException e) {
	    System.out.println("Don`t correct format!!!");
	    return false;
	} catch(NullPointerException e) {
	    System.out.println("Empty string!!!");
	    return false;
	}
	return true;
    }
}